1. Do you prefer vuejs or reactjs? Why ?

I prefer react js because react is a JavaScript library. It’s not a framework, when we are talking about vue it means a framework javascript where we need to know some specific templates engine to make a declarative ui, but if i use react i dont need to know some specific template engine because its pure html inside javascript, its called jsx

2. What complex things have you done in frontend development ?

I've ever made a realtime chatapp like a slack app with react js, mobx with websocket actioncable from ruby on rails

3. why does a UI Developer need to know and understand UX? how far do you understand it?

The reason is sometimes ui developer transforming design into code, keep thinking how user is going to use it the app, is it easy to use or not?, is it the mobile version proper enough for the user? or wondering why the design should like this

5. Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!

  link preview : https://relaxed-edison-8a4fce.netlify.app/

  source code : https://gitlab.com/fills27/paket-login.git

4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!

My analisis about https://taskdev.mile.app/login
  - Ui is good, the color is soft but the image background little bit blurry
  - Ux is good, but little bit confused about back button on dekstop version

6. Logic

  a.

    var a = 3, b = 5 
    b = [a, a = b][0]

  b.

    var a = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100],
      count = 100,
      missing = []

    for (var i = 1; i <= count; i++) {
      if (a.indexOf(i) == -1) {
        missing.push(i)
      }
    }

  c.

    function findDuplicates(arr) {
      var len=arr.length,
          out=[],
          counts={}

      for (var i=0;i<len;i++) {
        var item = arr[i]
        counts[item] = counts[item] >= 1 ? counts[item] + 1 : 1
        if (counts[item] === 2) {
          out.push(item)
        }
      }

      return out
    }

    var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
    
    findDuplicates(arr)

  d.

    function groupArrToObject(arr) {
      const groups = (arr, i) => {
        const obj = arr.reduce((groups, data) => {
          const place =
            i === 0 ? data[0] : typeof data[2] === "undefined" ? data[0] : data[2];
          if (!groups[place]) {
            groups[place] = [];
          } else if (data.length > 4) {
            groups[place].push(data);
          }
          return groups;
        }, {});
        return obj;
      };

      const newData = groups(arr, 0);

      for (let i = 0; i < Object.keys(newData).length; i++) {
        let x = i + 1;
        newData[x] = groups(arr, x);
        for (x; x <= Object.keys(groups(arr, x)).length; x++) {
          let newArr = groups(arr, x)[x];
          let rv = {};
          for (let z = 0; z < newArr.length; ++z) {
            rv[newArr[z].substr(newArr[z].length - 2)[0]] = newArr[z];
          }
          newData[i + 1][x] = rv;
        }
      }

      return newData;
    }

    let arr = [
      "1.",
      "1.1.",
      "1.2.",
      "1.3.",
      "1.4.",
      "1.1.1.",
      "1.1.2.",
      "1.1.3.",
      "1.2.1.",
      "1.2.2.",
      "1.3.1.",
      "1.3.2.",
      "1.3.3.",
      "1.3.4.",
      "1.4.1.",
      "1.4.3."
    ];

    groupArrToObject(arr)